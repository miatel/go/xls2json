package xls2json_test

import (
	"os"
	"testing"

	"xls2json"
)

type testFilePair struct {
	fileName string
	options  []xls2json.Option
	json     string
	result   bool
}

func TestNewCSV(t *testing.T) {
	_ = xls2json.NewCSV(
		xls2json.WithDelimiter(';'),
	)
}

func parseFile(t *testing.T, fileName string, options ...xls2json.Option) (json []byte, err error) {
	var file *os.File

	file, err = os.Open(fileName)
	if err != nil {
		t.Fatalf("Open file %s error: %s", fileName, err)
	}
	defer file.Close()

	p := xls2json.NewCSV(options...)
	json, err = p.Parse(file)
	if err != nil {
		return nil, err
	}

	return json, nil
}

func TestCSV_Parse(t *testing.T) {
	var testFilePairs = []testFilePair{
		{
			fileName: "test_data/csv/empty.csv",
			options: []xls2json.Option{
				xls2json.WithDelimiter(';'),
			},
			json:   "[]",
			result: true,
		},
		{
			fileName: "test_data/csv/one_row.csv",
			options: []xls2json.Option{
				xls2json.WithDelimiter(';'),
			},
			json:   "[[\"aaa\",\"bbb\",\"\"]]",
			result: true,
		},
		{
			fileName: "test_data/csv/two_row.csv",
			options: []xls2json.Option{
				xls2json.WithDelimiter(';'),
			},
			json:   "[[\"aaa\",\"ccc\",\"\"],[\"bbb\",\"ddd\",\"\"]]",
			result: true,
		},
		{
			fileName: "test_data/csv/two_row_broken.csv",
			options: []xls2json.Option{
				xls2json.WithDelimiter(';'),
			},
			result: false,
		},
	}

	for _, pair := range testFilePairs {
		json, err := parseFile(t, pair.fileName, pair.options...)
		if err != nil {
			if pair.result {
				t.Errorf("Parse file %s error: %s", pair.fileName, err)
			}
			continue
		}
		if (string(json) == pair.json) != pair.result {
			t.Error(pair.fileName+":", string(json), "not equal", pair.json)
		}
	}
}
