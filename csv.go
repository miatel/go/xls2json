package xls2json

import (
	"encoding/csv"
	"io"

	"github.com/mailru/easyjson"
)

type CSV struct {
	delimiter rune
}

func NewCSV(options ...Option) *CSV {
	optionsHash := &Options{
		Delimiter: ',',
	}
	for _, opt := range options {
		opt.Apply(optionsHash)
	}
	return &CSV{optionsHash.Delimiter}
}

func (p *CSV) Parse(r io.Reader) ([]byte, error) {
	var (
		err  error
		data tableArray
	)

	reader := csv.NewReader(r)
	reader.Comma = p.delimiter
	reader.Comment = '#'
	if data, err = reader.ReadAll(); err != nil {
		return nil, err
	}

	if len(data) == 0 {
		return []byte("[]"), nil
	}

	return easyjson.Marshal(data)
}
