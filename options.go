package xls2json

type Option interface {
	Apply(options *Options)
}

type Options struct {
	Delimiter rune
}

type withDelimiter struct {
	Delimiter rune
}

func (o withDelimiter) Apply(options *Options) {
	options.Delimiter = o.Delimiter
}

func WithDelimiter(Delimiter rune) Option {
	return withDelimiter{Delimiter}
}
