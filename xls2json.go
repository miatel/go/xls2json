package xls2json

import "io"

type Parser interface {
	Parse(r io.Reader, mode string) ([]byte, error)
}

//easyjson:json
type tableArray [][]string
